package org.database;

import org.domain.User;
import java.sql.*;

public class ConnectionMysql {

    private static Connection getConnection() {
        try {
            String driver = Config.DRIVER_NAME;
            String url = "jdbc:mysql://"+Config.DB_HOST+":"+
                    Config.DB_PORT+"/"+Config.DB_NAME+
                    "?useLegacyDatetimeCode=false&serverTimezone=UTC";
            String username = Config.DB_USERNAME;
            String pass = Config.DB_PASSWORD;
//            String driver = "com.mysql.cj.jdbc.Driver";
//            String url = "jdbc:mysql://localhost:3306/mydb?useLegacyDatetimeCode=false&serverTimezone=UTC";
//            String username = "root";
//            String pass = "";

            Class.forName(driver);
            return DriverManager.getConnection(url, username, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void createTable() throws SQLException {
        Connection con = getConnection();
        Statement st = con.createStatement();
        String createTable = "create table if not exists " + Config.TABLE_NAME+"(" +
                 Config.USER_ID + " int primary key auto_increment, " +
                 Config.USER_NAME + " varchar(100) not null, " +
                 Config.USER_SURNAME +" varchar(100) not null, " +
                 Config.USER_USERNAME +" varchar(50) not null, " +
                 Config.USER_PASSWORD +" varchar(100) not null, " +
                 Config.USER_EMAIL + " varchar(100) not null, " +
                 Config.USER_COUNTRY + " varchar(100) not null, " +
                 Config.USER_CITY + " varchar(50) not null, " +
                 Config.USER_PHONE_NUMBER + " varchar(12) not null, " +
                "unique(" + Config.USER_USERNAME+ "," + Config.USER_EMAIL + "," + Config.USER_PHONE_NUMBER + "));";

//        String createTable = "create table if not exists users(" +
//                 "id int primary key auto_increment, " +
//                 "name varchar(100) not null, " +
//                 "surname varchar(100) not null, " +
//                 "username varchar(50) not null, " +
//                 "password varchar(100) not null, " +
//                 "email varchar(100) not null, " +
//                 "country varchar(100) not null, " +
//                 "city varchar(50) not null, " +
//                 "phone_number varchar(12) not null, " +
//                "unique(username, email, phone_number));";

        st.executeUpdate(createTable);
        System.out.println("Table have created");
    }

    public static void signUpUser(User user) {
        String insert = "INSERT INTO " + Config.TABLE_NAME +"("+
                Config.USER_NAME + "," + Config.USER_SURNAME + ","+
                Config.USER_USERNAME + "," + Config.USER_PASSWORD + "," +
                Config.USER_EMAIL + "," + Config.USER_COUNTRY + "," +
                Config.USER_CITY + "," + Config.USER_PHONE_NUMBER + ")" +
                "VALUES(?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement statement = getConnection().prepareStatement(insert);
            statement.setString(1, user.getName());
            statement.setString(2, user.getSurname());
            statement.setString(3, user.getUsername());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getCountry());
            statement.setString(7, user.getCity());
            statement.setString(8, user.getPhoneNumber());
            statement.executeUpdate();
            System.out.println("User added");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ResultSet getUser(User user) {
        ResultSet resultSet = null;

        String select = "SELECT * FROM " + Config.TABLE_NAME + " WHERE " +
                Config.USER_USERNAME + "=? AND " + Config.USER_PASSWORD + "=?";

        try {
            PreparedStatement prepSt = getConnection().prepareStatement(select);
            prepSt.setString(1, user.getUsername());
            prepSt.setString(2, user.getPassword());

            resultSet = prepSt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }
}
