package org.database;

public class Config {
    public static final String TABLE_NAME = "users";

    public static final String USER_ID = "id";
    public static final String USER_NAME = "name";
    public static final String USER_SURNAME = "surname";
    public static final String USER_USERNAME = "username";
    public static final String USER_PASSWORD = "password";
    public static final String USER_EMAIL = "email";
    public static final String USER_COUNTRY = "country";
    public static final String USER_CITY = "city";
    public static final String USER_PHONE_NUMBER = "phone_number";

    public static final String DB_HOST = "localhost";
    public static final String DB_PORT = "3306";
    public static final String DB_USERNAME = "root";
    public static final String DB_PASSWORD = "";
    public static final String DB_NAME = "mydb";

    public static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";

}
