package org.example;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ProfileController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button signOut_button;

    @FXML
    private Label name_label;

    @FXML
    private Label surname_label;

    @FXML
    private Label username_label;

    @FXML
    private Label country_field;

    @FXML
    private Label city_field;

    @FXML
    private Label phone_number;

    @FXML
    private Label email_field;

    @FXML
    void initialize() {
        signOut_button.setOnAction(event -> {
            try {
                App.setRoot("signin");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
