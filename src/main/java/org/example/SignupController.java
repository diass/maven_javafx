package org.example;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.database.ConnectionMysql;
import org.domain.Password;
import org.domain.User;

public class SignupController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField name_field;

    @FXML
    private PasswordField password_field;

    @FXML
    private Button signUp_button;


    @FXML
    private TextField surname_field;

    @FXML
    private TextField username_field;

    @FXML
    private TextField email_field;

    @FXML
    private TextField country_field;

    @FXML
    private TextField phoneNumber_field;

    @FXML
    private Button sendCode_button;

    @FXML
    private TextField verification_field;

    @FXML
    private TextField city_field;

    @FXML
    private Button back_button;

    @FXML
    void initialize() {
        verification_field.setDisable(true);

        back_button.setOnAction(event -> {
            try {
                App.setRoot("signin");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        signUp_button.setOnAction((event -> {
            try {
                if(!isFormFilled()) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("WARNING");
                    alert.setHeaderText("Fill all inputs!");
                    alert.showAndWait();
                }
                else {
                    signUpNewUser();
                    App.setRoot("signin");
                }
            } catch (InvalidKeySpecException | IOException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

        }));
    }

    private void signUpNewUser() throws InvalidKeySpecException, NoSuchAlgorithmException {
        String firstName = name_field.getText().trim();
        String surname = surname_field.getText().trim();
        String username = username_field.getText().trim();
        String password = password_field.getText().trim();
        String email = email_field.getText().trim();
        String country = country_field.getText().trim();
        String city = city_field.getText().trim();
        String phoneNumber = phoneNumber_field.getText().trim();

        User user = new User(firstName, surname, username, new Password(password) , email, country, city, phoneNumber);

        ConnectionMysql.signUpUser(user);
    }

    private boolean isFormFilled() {
        return !name_field.getText().trim().isEmpty() &&
                !surname_field.getText().trim().isEmpty() &&
                !username_field.getText().trim().isEmpty() &&
                !password_field.getText().trim().isEmpty() &&
                !email_field.getText().trim().isEmpty() &&
                !country_field.getText().trim().isEmpty() &&
                !city_field.getText().trim().isEmpty() &&
                !phoneNumber_field.getText().trim().isEmpty();
    }
}
