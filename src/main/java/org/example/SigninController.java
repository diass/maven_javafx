package org.example;

import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.database.ConnectionMysql;
import org.domain.Password;
import org.domain.User;

public class SigninController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField username_field;

    @FXML
    private PasswordField password_field;

    @FXML
    private Button signin_button;

    @FXML
    private Button signup_button;

    @FXML
    private void switchToSinup() throws IOException {
        App.setRoot("signup");
    }

    @FXML
    void initialize() {
        signin_button.setOnAction(event -> {
            try {
                loginUser(username_field.getText(), password_field.getText());
            } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        });

    }

    private void loginUser(String username, String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        User user = new User();
        user.setUsername(username);
        user.setPassword(new Password(password));
        ResultSet result = ConnectionMysql.getUser(user);

        int counter = 0;

        try {
            while (result.next()) {
                counter++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(counter >= 1) {
            try {
                App.setRoot("profile");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("No such user");
        }
    }
}
