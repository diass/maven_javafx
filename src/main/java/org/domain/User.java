package org.domain;

public class User {
    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;
    private String username;
    private Password password;
    private String email;
    private String country;
    private String city;
    private String phoneNumber;

    public User(){ id = id_gen++;  }

    public User(String name, String surname, String username,Password password,String email, String country, String city, String phoneNumber ){
        this();
        setName(name);
        setSurname(surname);
        setUsername(username);
        setPassword(password);
        setEmail(email);
        setCountry(country);
        setCity(city);
        setPhoneNumber(phoneNumber);
    }

    public String getName(){ return name; }

    public String getSurname() { return surname; }

    public String getUsername() { return username; }

    public String getPassword(){
        return password.getPassword();
    }

    public String getEmail() { return email; }

    public String getCountry() { return country; }

    public String getCity() { return city; }

    public String getPhoneNumber() { return phoneNumber; }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(Password password) { this.password = password; }

    public void setEmail(String email) { this.email = email; }

    public void setCountry(String country) { this.country = country; }

    public void setCity(String city) { this.city = city; }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
