package org.domain;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class Password {
    private static int PASS_LENGTH = 9;
    private String passwordHash;
    private String password;

//    public Password(String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
//        setPasswordHash(password);
//    }

    public Password(String password) {
        setPass(password);
    }

    public String getPasswordHash(){
        return passwordHash;
    }

    public String getPassword() {
        return password;
    }

    public void setPasswordHash(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        if(checkPassword(password)){

            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[16];
            random.nextBytes(salt);

            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");

//            passwordHash = factory.generateSecret(spec).hashCode();
            byte[] hash = factory.generateSecret(spec).getEncoded();
            passwordHash = String.valueOf(hash.hashCode());
        } else {
            System.out.println("Wrong password!");
        }
    }

    public void setPass(String password) {
        this.password = password;
    }

//    public boolean checkHashcode(String password, User user) throws InvalidKeySpecException, NoSuchAlgorithmException {
//
//    }

    private boolean checkPassword(String password){
        if(password.length() < PASS_LENGTH){
            return false;
        } else {
            char character;
            int lowCharacter = 0;
            int upperCharacter = 0;
            int digits = 0;
            for (int i = 0; i < password.length(); i++){
                character = password.charAt(i);
                if(Character.isLetter(character)){
                    if(Character.isUpperCase(character))
                        upperCharacter++;
                    if(Character.isLowerCase(character))
                        lowCharacter++;
                }
                if(Character.isDigit(character))
                    digits++;
            }
            if((lowCharacter < 1) && (upperCharacter < 1) && (digits < 1))
                return false;
        }
        return true;
    }
}